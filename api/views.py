from django.db.models import Max
from rest_framework import generics, permissions, filters, pagination

from api.serializers import ComplaintsSerializer, ContactSubjectSerializer, ContactTypeSerializer
from complaints.models import Complaints, ContactType, ContactSubject
from django_filters import rest_framework as rest_filters
from api.serializers import MessageSerializer, UserSerializer
from msg.models import Messages
from user.models import Users


class ComplaintsFilter(rest_filters.FilterSet):
    report_date = rest_filters.DateFromToRangeFilter(name="report_date")

    class Meta:
        model = Complaints
        fields = ("report_date", 'app_version', 'type', 'subtype')


class ComplaintsListApiView(generics.ListAPIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = ComplaintsSerializer
    filter_class = ComplaintsFilter
    filter_backends = (rest_filters.DjangoFilterBackend, filters.OrderingFilter, filters.SearchFilter)
    pagination_class = pagination.LimitOffsetPagination
    ordering_fields = "__all__"
    search_fields = ("reported_by", "report_date", "reason", "subject", "detail", "app_version")

    def get_queryset(self):
        queryset = Complaints.objects.exclude(reported_by=None).values(
            "reported_by").annotate(report_date=Max("report_date"))
        return Complaints.objects.filter(
            reported_by__in=(x["reported_by"] for x in queryset)).filter(
            report_date__in=(x["report_date"] for x in queryset)).order_by("-report_date")


class ContactSubjectListApiView(generics.ListAPIView):
    queryset = ContactSubject.objects.all()
    permission_classes = (permissions.AllowAny,)
    serializer_class = ContactSubjectSerializer


class ContactTypeListApiView(generics.ListAPIView):
    queryset = ContactType.objects.all()
    permission_classes = (permissions.AllowAny,)
    serializer_class = ContactTypeSerializer


class MessageListApiView(generics.ListCreateAPIView):
    queryset = Messages.objects.all()
    permission_classes = (permissions.AllowAny,)
    serializer_class = MessageSerializer
    filter_backends = (filters.SearchFilter,)
    search_fields = '__all__'


class MessageRetrieveUpdateApiView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Messages.objects.all()
    permission_classes = (permissions.AllowAny,)
    serializer_class = MessageSerializer


class UserListApiView(generics.ListCreateAPIView):
    queryset = Users.objects.all()
    permission_classes = (permissions.AllowAny,)
    serializer_class = UserSerializer
    filter_backends = (filters.SearchFilter,)
    search_fields = ('username', 'email')

    def get_queryset(self):
        queryset = super(UserListApiView, self).get_queryset().filter(is_active=True).exclude(id=self.request.user.id)
        return queryset


class UserRetrieveUpdateApiView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Users.objects.all()
    permission_classes = (permissions.AllowAny,)
    serializer_class = UserSerializer
