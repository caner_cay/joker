from django.conf.urls import url

from api.views import (ComplaintsListApiView, ContactSubjectListApiView, ContactTypeListApiView)
from api.views import (UserListApiView, UserRetrieveUpdateApiView, MessageListApiView)

urlpatterns = [
    url(r'^complaints/$', ComplaintsListApiView.as_view()),
    url(r'^contact_type/$', ContactTypeListApiView.as_view()),
    url(r'^contact_subject/$', ContactSubjectListApiView.as_view()),
    url(r'^users/$', UserListApiView.as_view()),
    url(r'^users/(?P<pk>[0-9]+)/$', UserRetrieveUpdateApiView.as_view()),
    url(r'^message/$', MessageListApiView.as_view()),
    url(r'^message/$', MessageListApiView.as_view()),
]
