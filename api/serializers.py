from rest_framework import serializers
from complaints.models import Complaints, ContactSubject, ContactType
from msg.models import Messages
from user.models import Users


class ComplaintsSerializer(serializers.ModelSerializer):
    closed = serializers.ReadOnlyField()
    total_contacts = serializers.ReadOnlyField()
    status = serializers.ReadOnlyField()
    payment_status = serializers.ReadOnlyField()
    membership_status = serializers.ReadOnlyField()
    action = serializers.ReadOnlyField()

    class Meta:
        model = Complaints
        fields = "__all__"


class ContactSubjectSerializer(serializers.ModelSerializer):
    pagination_class = None

    class Meta:
        model = ContactSubject
        fields = "__all__"


class ContactTypeSerializer(serializers.ModelSerializer):
    pagination_class = None
    paginate_by = None

    class Meta:
        model = ContactType
        fields = "__all__"


class UserSerializer(serializers.ModelSerializer):
    one_signal_id = serializers.CharField()

    class Meta:
        model = Users
        fields = ('username', 'one_signal_id', 'password', 'email')
        extra_kwargs = {
            'password': {'write_only': True},
        }


class MessageSerializer(serializers.ModelSerializer):
    receiver = UserSerializer(read_only=True)
    sender = UserSerializer(read_only=True)

    receiver_id = serializers.PrimaryKeyRelatedField(
        queryset=Users.objects.all(), source='receiver', write_only=True)

    sender_id = serializers.PrimaryKeyRelatedField(
        queryset=Users.objects.all(), source='sender', write_only=True)

    class Meta:
        model = Messages
        fields = "__all__"
