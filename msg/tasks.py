import datetime
import logging

from celery.schedules import crontab
from celery.task.base import periodic_task

from msg.models import Messages

logger = logging.getLogger('msg.tasks')


@periodic_task(run_every=crontab(hour=0, minute=5))
def expire_account():
    logger.info('Started')
    for message in Messages.objects.filter(receiver__sent_date__lt=datetime.date.today()):
        message.push_notification()
