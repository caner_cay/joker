import datetime

from django.conf import settings
from django.db import models
from onesignalclient.app_client import OneSignalAppClient
from onesignalclient.notification import Notification
from requests.exceptions import HTTPError

from user.models import Users


class Messages(models.Model):
    message = models.TextField()
    sent_date = models.DateTimeField(blank=False, default=datetime.date.today)
    sender = models.ForeignKey(Users, related_name='sender')
    receiver = models.ForeignKey(Users, related_name='receiver')

    def push_notification(self):
        client = OneSignalAppClient(app_id=getattr(settings, 'OS_APP_ID', None),
                                    app_api_key=getattr(settings, 'OS_APP_KEY', None))

        # Creates a new notification
        notification = Notification(getattr(settings, 'OS_APP_ID', None), Notification.DEVICES_MODE)
        notification.include_player_ids = [self.receiver.one_signal_id, ]

        try:
            result = client.create_notification(notification)
        except HTTPError as e:
            result = e.response.json()
