from django.db import connection, models
from django.db.models import Manager, Count, Sum

from user.models import GameUser


class MyManager(Manager):
    def raw_as_qs(self, raw_query, params=()):
        """Execute a raw query and return a QuerySet.  The first column in the
        result set must be the id field for the model.
        :type raw_query: str | unicode
        :type params: tuple[T] | dict[str | unicode, T]
        :rtype: django.db.models.query.QuerySet
        """
        cursor = connection.cursor()
        try:
            cursor.execute(raw_query, params)
            return self.filter(reported_by__in=(x[1] for x in cursor)).filter(
                report_date__in=(x[0] for x in cursor)).exclude(reported_by=None)
        finally:
            cursor.close()


class Complaints(models.Model):
    complaint_type = models.IntegerField()
    reported_by = models.CharField(max_length=255)
    reported = models.CharField(max_length=255)
    reason = models.CharField(max_length=255)
    subject = models.CharField(max_length=255)
    email = models.CharField(max_length=255)
    detail = models.TextField()
    report_date = models.DateTimeField()
    file = models.CharField(max_length=255)
    replied = models.IntegerField()
    chat_log = models.TextField()
    app_version = models.CharField(max_length=255)
    type = models.IntegerField()
    subtype = models.IntegerField()
    reply_date = models.DateTimeField()
    action_status = models.IntegerField()
    related_answer_id = models.IntegerField()
    objects = MyManager()

    class Meta:
        db_table = "complaints"

    def __str__(self):
        return '{0} ,, {1}'.format(self.reported_by, self.subject)

    @property
    def action(self):
        if hasattr(self, 'action'):
            return self.action
        return None

    @property
    def membership_status(self):
        if hasattr(self, 'membership_status'):
            return self.action
        return None

    @property
    def payment_status(self):
        if hasattr(self, 'payment_status'):
            return self.action
        return None

    @property
    def total_contacts(self):
        closed = Complaints.objects.filter(reported_by=self.reported_by).aggregate(
            Count("reported_by"))
        if closed["reported_by__count"] is None:
            return 0
        else:
            return closed["reported_by__count"]

    @property
    def closed(self):
        closed = Complaints.objects.filter(reported_by=self.reported_by).aggregate(
            Sum("replied"))
        if closed["replied__sum"] is None:
            return 0
        else:
            return closed["replied__sum"]

    @property
    def status(self):
        if self.total_contacts == self.closed:
            return "Closed"
        elif self.closed == 0:
            return "Open"
        else:
            return "Re-open"

    @property
    def payment_status(self):
        if GameUser.objects.filter(facebookId=self.reported_by).count() > 0:
            user = GameUser.objects.filter(facebookId=self.reported_by).first()
            if user.wp.count() > 0 or user.ios.count() > 0 or user.android.count() > 0 or user.facebook.count() > 0:
                return "Payer User"
            else:
                return "Free User"
        else:
            return "Free User"

    @property
    def membership_status(self):
        if GameUser.objects.filter(facebookId=self.reported_by).count() > 0:
            user = GameUser.objects.filter(facebookId=self.reported_by).first()
            if user.user_role == 1:
                return "VIP	User"
            else:
                return "Standart User"
        else:
            return "Standart User"

    @property
    def action(self):
        return self.reported_by


class ContactSubject(models.Model):
    name = models.CharField(max_length=255)
    lang = models.CharField(max_length=32)
    type = models.IntegerField()
    order = models.IntegerField()

    class Meta:
        db_table = "contact_subject"

    def __str__(self):
        return '{0} ,, {1}'.format(self.name, self.lang)


class ContactType(models.Model):
    name = models.CharField(max_length=255)
    lang = models.CharField(max_length=32)
    order = models.IntegerField()

    class Meta:
        db_table = "contact_type"

    def __str__(self):
        return '{0} ,, {1}'.format(self.name, self.lang)
