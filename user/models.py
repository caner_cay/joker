from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import ugettext_lazy as _

from main.contrib import send_template_email, get_user_language


def photo_dir(instance, filename):
    return "/".join(["profile", instance.user.username, filename])


class Users(AbstractUser):
    one_signal_id = models.CharField(max_length=150,
                                     unique=True,
                                     help_text=_(
                                         'Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
                                     error_messages={
                                         'unique': _("A user with that one signal id already exists."),
                                     }, )

    def save(self, *args, **kwargs):
        create = False
        if self.pk is None:
            create = True
        super(Users, self).save(*args, **kwargs)

        mail_context = ({'user': self.user})
        if create:
            send_template_email([self.user.email, ], 'mail/welcome_title.html', 'mail/welcome_body.html',
                                mail_context, get_user_language(self.user))


class GameUser(models.Model):
    firstname = models.CharField(_('first name'), max_length=30, blank=True)
    lastname = models.CharField(_('last name'), max_length=30, blank=True)
    email = models.EmailField(_('email address'), blank=True)
    last_login = models.DateTimeField()
    facebookId = models.CharField(max_length=255, unique=True)
    location = models.CharField(max_length=255)
    level = models.IntegerField()
    score = models.BigIntegerField()
    last_daily_earning = models.DateTimeField()
    install_date = models.DateTimeField()
    gender = models.CharField(max_length=255)
    birthday = models.DateTimeField()
    total_exp = models.IntegerField()
    user_role = models.IntegerField()
    role_expire_date = models.DateTimeField()
    user_settings = models.IntegerField()
    current_language = models.CharField(max_length=32)
    has_flag = models.SmallIntegerField()

    class Meta:
        db_table = "game_user"

    def __str__(self):
        return '{0} {1}'.format(self.firstname, self.lastname)
