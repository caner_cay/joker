from django.db import models
from user.models import GameUser as User


class WP(models.Model):
    original_purchase_date = models.CharField(max_length=65)
    user = models.ForeignKey(User, related_name='wp')
    price = models.FloatField(default=0)
    server_time = models.DateTimeField()
    currency = models.CharField(max_length=3, default='TRY')

    class Meta:
        db_table = 'wp_payment'


class IOS(models.Model):
    original_purchase_date = models.CharField(max_length=65)
    status = models.DateTimeField()
    user = models.ForeignKey(User, related_name='ios')
    price = models.FloatField(default=0)
    server_time = models.DateTimeField()
    currency_code = models.CharField(max_length=255)
    is_production = models.IntegerField()

    class Meta:
        db_table = 'ios_payment'


class Android(models.Model):
    purchase_time = models.DateTimeField()
    purchase_state = models.SmallIntegerField()
    user = models.ForeignKey(User, related_name='android')
    price = models.FloatField(default=0)
    server_time = models.DateTimeField()
    currency_code = models.CharField(max_length=255)

    class Meta:
        db_table = 'android_payment'


class Facebook(models.Model):
    user_fb_id = models.CharField(max_length=255)
    user_db = models.ForeignKey(User, related_name='facebook')
    time_updated = models.DateTimeField(auto_now_add=True)
    time_created = models.DateTimeField(auto_now=True)
    server_time = models.DateTimeField()
    price = models.FloatField(default=0)
    is_test = models.IntegerField()
    currency = models.CharField(max_length=3)
    country_code = models.CharField(max_length=2)
    action_type = models.CharField(max_length=45)

    class Meta:
        db_table = 'facebook_payment'
